import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import ComponentContainer from './react/component';
import { appStore } from './redux/synchronRedux';
declare let module: any

ReactDOM.render(
  <Provider store={appStore}>
    <ComponentContainer />
  </Provider>,
  document.getElementById('root')
);


if (module.hot) {
   module.hot.accept();
}
