import * as React from 'react';
import { ApplicationState, add } from '../redux/synchronRedux';
import { Action, Dispatch } from 'redux';
import { connect } from 'react-redux';

interface ComponentState {
  numberToAdd: string;
}

interface StateProps {
  number: number
}

interface DispatchProps {
  addNumber: (number: number) => void;
}

const mapStateToProps = (state: ApplicationState): StateProps => {
  return {
    number: state.firstApplicationReducer.number
  };
};

const mapDispatchToProps = (dispatch: Dispatch<Action>) => {
  return {
    addNumber: (number: number) => { dispatch(add(number)) }
  };
};

export class Component extends React.Component<DispatchProps & StateProps, ComponentState> {
  constructor(props: DispatchProps & StateProps) {
    super(props);
    this.state = {
      numberToAdd: ""
    };
  }

  onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      numberToAdd: event.target.value
    });
  };

  onCkickAdd = () => {
    this.props.addNumber(+this.state.numberToAdd)
    this.setState({
      numberToAdd: ""
    });
  };

  render() {
    return (
      <div>
        <h1>{this.props.number}</h1>
        <input type="text" value={this.state.numberToAdd} onChange={this.onChange} />
        <button onClick={this.onCkickAdd}>Add</button>
      </div>
    )
  };
}

let ComponentContainer = connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(Component);
export default ComponentContainer;
