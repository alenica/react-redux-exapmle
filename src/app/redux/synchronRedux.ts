import { createStore, combineReducers } from 'redux';
import { Action } from "redux";

//состояние всего приложения
export interface ApplicationState {
  firstApplicationReducer: FirstReducerState
}

export interface FirstReducerState {
  number: number;
}

//начальное состояние, связанное с первым reducer-ом
let initialFirstReducerState: FirstReducerState = {
  number: 0
};

//начальное состояние всего приложения
let initialApplicationState: ApplicationState = {
  firstApplicationReducer: initialFirstReducerState
}

//типизированное действие - action
export const ADD = 'ADD';
export const add = (number: number) => {
  let action: AddAction = {
    type: ADD,
    number: number
  }
  return action;
}
export interface AddAction extends Action {
  type: string;
  number: number;
}

//функция-обрабочик действий, reducer
export default function firstApplicationReducer(state: FirstReducerState = initialFirstReducerState, action: Action): FirstReducerState {
  switch (action.type) {
    case ADD: {
      let typedAction: AddAction = <AddAction>action;
      return {
        number: state.number + typedAction.number
      };
    };
    default: {
      return state;
    }
  }
}

//комбинация всех reducer-ов
const combineReducer = combineReducers({ firstApplicationReducer });

//хранилище, то, что хранит состояние и позволяет применять action-ы
export const appStore = createStore(
  combineReducer,
  initialApplicationState
)
